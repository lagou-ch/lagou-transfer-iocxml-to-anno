import com.ch.edu.dao.AccountDao;
import com.ch.edu.factory.BeanFactoryByAnno;
import com.ch.edu.service.TransferService;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;


public class IocTest {

    @Test
    public void test() throws Exception {
        /*ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("classpath:applicationContext.xml");
        AccountDao accountDao = (AccountDao) applicationContext.getBean("accountDao");
        System.out.println(accountDao);
        applicationContext.close();
       */
        // 测试代码
        TransferService transferService = (TransferService) BeanFactoryByAnno.getBean("transferService") ;
        transferService.transfer("6029621011000","6029621011001",100);
    }
}
