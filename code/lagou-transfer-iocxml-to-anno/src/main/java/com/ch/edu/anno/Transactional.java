package com.ch.edu.anno;

import java.lang.annotation.*;

@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
@SuppressWarnings("all")
public @interface Transactional {

    /**
     * Alias for {@link #transactionManager}.
     *
     * @see #transactionManager
     */
    String value() default "transactionManager";
}