package com.ch.edu.anno;

import org.springframework.stereotype.Component;

import java.lang.annotation.*;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Component
@SuppressWarnings("all")
public @interface Service {

    String value() default "";

}