package com.ch.edu.service.impl;

import com.ch.edu.pojo.Account;
import com.ch.edu.anno.Autowired;
import com.ch.edu.anno.Service;
import com.ch.edu.anno.Transactional;
import com.ch.edu.dao.AccountDao;
import com.ch.edu.service.TransferService;

/**
 * @author 应癫
 */

@Service("transferService")
@Transactional
@SuppressWarnings(value = "all")
public class TransferServiceImpl implements TransferService {

    // 最佳状态
    @Autowired
    private AccountDao accountDao;

    public void setAccountDao(AccountDao accountDao) {
        this.accountDao = accountDao;
    }


    @Override
    public void transfer(String fromCardNo, String toCardNo, int money) throws Exception {

            Account from = accountDao.queryAccountByCardNo(fromCardNo);
            Account to = accountDao.queryAccountByCardNo(toCardNo);

            from.setMoney(from.getMoney()-money);
            to.setMoney(to.getMoney()+money);

            accountDao.updateAccountByCardNo(to);
            // 人为异常
            //int c = 1/0;
            accountDao.updateAccountByCardNo(from);

    }
}
