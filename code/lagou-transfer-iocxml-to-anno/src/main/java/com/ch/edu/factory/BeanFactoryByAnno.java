package com.ch.edu.factory;

import com.ch.edu.anno.Autowired;
import com.ch.edu.anno.Service;
import com.ch.edu.anno.Transactional;
import org.reflections.Reflections;
import org.springframework.util.StringUtils;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

/**
 *
 * @author chen
 *
 * 工厂类，生产对象（使用反射技术，解析自定义注解，实现依赖注入和bean的实例化）
 */
@SuppressWarnings("all")
public class BeanFactoryByAnno {


    private static Map<String,Object> map = new HashMap<>();

    /**
     * 1.扫描所有的类
     * 2.获取注解，（Repository替换Service）
     *      a.先获取Service的注解的类，
     *      b.然后扫描Autowoired，判断是否存在DI注入
     *      c.最后事务进行代理
     */
    static {
        // 扫描获取反射对象集合
        Reflections rf = new Reflections("com.ch.edu");
        Set<Class<?>> serviceAnnotations = rf.getTypesAnnotatedWith(Service.class);
        serviceAnnotations.forEach(o ->
        {
            try {
                Object bean = o.newInstance();
                Class<?> target = bean.getClass();
                Service annotation = target.getAnnotation(Service.class);
                // 判断注解上是否存在value属性值
                if (annotation != null && StringUtils.isEmpty(annotation.value()) ) {
                    String[] split = o.getName().split("\\.");
                    map.put(split[split.length - 1], bean);
                }else {
                    map.put(annotation.value(), bean);
                }
            } catch (InstantiationException  | IllegalAccessException e) {
                e.printStackTrace();
            }
        });

        // 实例化完成之后维护对象的依赖关系
        Set<Map.Entry<String, Object>> entries = map.entrySet();
        entries.forEach(o ->
        {
            // 获取注解了service的类
            Object value = o.getValue();
            Class<?> aClass = value.getClass();
            // 获取类下的所有自断
            List<Field> fields = Arrays.asList(aClass.getDeclaredFields());
            Object finalValue = value;
            fields.forEach(f-> {
                // 判断对象是否持有Autowired注解
                if (f.isAnnotationPresent(Autowired.class)){
                    String[] split = f.getType().getName().split("\\.");
                    String name = split[split.length - 1];
                    // 遍历对象中的所有方法，找到"set" + name
                    List<Method> methods = Arrays.asList(aClass.getMethods());
                    methods.forEach(m->
                    {
                        if(m.getName().equalsIgnoreCase("set" + name)) {
                            try {
                                // 对该对象进行set属性-方法（传值bean对象）完成依赖关系的处理
                                m.invoke(finalValue, map.get(name));
                            } catch (IllegalAccessException e) {
                                e.printStackTrace();
                            } catch (InvocationTargetException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            });

            // 判断对象类是否持有Transactional注解，若有则修改对象为代理对象
            if(aClass.isAnnotationPresent(Transactional.class)){
                // 获取代理工厂
                ProxyFactory proxyFactory = (ProxyFactory) BeanFactoryByAnno.getBean("proxyFactory");
                // 判断对象是否实现接口
                Class[] face = aClass.getInterfaces();
                if(face!=null && face.length>0){
                    value = proxyFactory.getJdkProxy(value);
                }else{
                    value = proxyFactory.getCglibProxy(value);
                }
            }
            // 重新覆盖原来mao，1，set了属性的bean，2，进行事务代理之后的bean
            map.put(o.getKey(), value);
        });

    }


    // 任务二：对外提供获取实例对象的接口（根据id获取）
    public static  Object getBean(String id) {
        return map.get(id);
    }

}
